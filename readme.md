#Clone

```
git clone https://gitlab.com/huongnguyenvan/playlist-youtube.git youtube

```

#Install

This project using Google Api Client (php). Please install package before access.

```
composer install
```

Change $DEV_KEY
get it from [here](https://console.developers.google.com/apis) credentials

Change $playlist Youtube



#Run

Access link 
[http://localhost/youtube](http://localhost/youtube)