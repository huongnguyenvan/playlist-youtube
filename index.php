<?php
    Header('Content-Type: text/html; charset=UTF-8');
    require_once 'vendor/autoload.php';

    $playlist = ['PLMveC4LU1myU1SfyYa-ZKRqfuioAqmd4V'];
    $DEV_KEY = "AIzaSyABFnKRHeBLP0WeXP1Wr89JnhLf2g4_xx";

    $client = new \Google_Client();
    $client->setDeveloperKey($$DEV_KEY);
    $youtube = new \Google_Service_YouTube($client);
    
    $nextPageToken = '';
    $responses = [];
    
    $file = fopen("playlist.csv","w");
    $id = 1;
    do {
        $playlistItemsResponse = $youtube->playlistItems->listPlaylistItems('snippet', array(
        'playlistId' => $playlist,
        'maxResults' => 50,
        'pageToken' => $nextPageToken));
    
        foreach ($playlistItemsResponse['items'] as $playlistItem) {
            fputcsv($file, 
                [
                    $id,
                    $playlistItem['snippet']['title'],
                    $playlistItem['snippet']['resourceId']['videoId'],
                    $playlistItem['snippet']['thumbnails']['standard']['url']
                ]
            );
            $id++;
        }
        $nextPageToken = $playlistItemsResponse['nextPageToken'];
    } while ($nextPageToken <> '');
    
    fclose($file);
    echo "Finish";